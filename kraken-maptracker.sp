#define PLUGIN_VERSION "1.0"

#define DEBUG

#include <sourcemod>
#include <sdkhooks>
#include <kraken-core>

#define SNAPSHOT_INTERVAL 300.0

bool g_bKrakenCore;
bool g_bLate;

Handle g_hTimer = null;

public Plugin myinfo =
{
	name = "Kraken - Map Tracker",
	author = "Zipcore",
	description = "Simple map player count tracker",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-map-stats");
	Kraken_MarkNativesAsOptional();
	
	g_bLate = bLate;

	return APLRes_Success;
}

public void OnPluginStart() 
{
	if(LibraryExists("kraken-core"))
		g_bKrakenCore = true;
	
	if(g_bLate)
	{
		g_bLate = false;
		CreateTable();
		InitTimer();
	}
}

public void OnLibraryAdded(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = false;
}

public Kraken_OnDatabaseConnected(Database dDatabase, int iSID, int iGID, char[] sServerName)
{
	CreateTable();
}

void CreateTable()
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
	
	Kraken_PushQuery("CREATE TABLE IF NOT EXISTS `map_tracker` (id INT NOT NULL AUTO_INCREMENT, sid INT NOT NULL, mid INT NOT NULL, players INT NOT NULL, date TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), INDEX mid_index USING BTREE (mid), INDEX sid_index USING BTREE (sid));");
}

public void Kraken_OnMapLoaded(int iMID)
{
	InitTimer();
}

void InitTimer()
{
	if(g_hTimer != null)
		delete g_hTimer;
	
	g_hTimer = CreateTimer(SNAPSHOT_INTERVAL, Timer_MapTime, _, TIMER_REPEAT);
}

public Action Timer_MapTime(Handle timer, any data)
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return Plugin_Continue;
	
	int iCount;
	for (int iClient = 1; iClient <= MaxClients; iClient++)
	{
		if(!Kraken_IsValidClient(iClient))
			continue;
		
		iCount++;
	}
	
	int iMapID = Kraken_GetMapID();
	int iServerID = Kraken_GetServerID();
	
	if(iMapID != -1 && iServerID != -1 && iCount > 0)
	{
		char sQuery[2048];
		Format(sQuery, sizeof(sQuery), "INSERT INTO map_tracker (mid, sid, players) VALUES (%d, %d, %d);", iServerID, iMapID, iCount);
		Kraken_PushQuery(sQuery);
	}
	
	return Plugin_Continue;
}