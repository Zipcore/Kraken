#define PLUGIN_VERSION "1.0"

#define DEBUG

#include <sourcemod>
#include <SteamWorks>
#include <sdkhooks>
#include <sdkhooks>
#include <geoip>
#include <multicolors>
#include <kraken-core>

#include "kraken/stocks.sp"
#include "kraken/globals.sp"
#include "kraken/convars.sp"
#include "kraken/boot.sp"
#include "kraken/database.sp"
#include "kraken/serversettings.sp"
#include "kraken/server.sp"
#include "kraken/settings.sp"
#include "kraken/version.sp"
#include "kraken/admin.sp"
#include "kraken/map.sp"
#include "kraken/player.sp"

public Plugin myinfo =
{
	name = "Kraken - Core",
	author = "Zipcore",
	description = "The Kraken is, specifically speaking, supposed to be a sea monster with no distinctive traits. However, it has become fixed with the image of a big, bad-ass squid that fucks up everything whenever it appears. So lets hope this thing doesn't.'",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-core");

	g_fwdOnDatabaseConnected = CreateGlobalForward("Kraken_OnDatabaseConnected", ET_Ignore, Param_Any, Param_Cell, Param_Cell, Param_String);
	g_fwdOnDatabasePong = CreateGlobalForward("Kraken_OnDatabasePong", ET_Ignore, Param_Any, Param_Cell, Param_Cell, Param_String);
	g_fwdOnMapLoaded = CreateGlobalForward("Kraken_OnMapLoaded", ET_Ignore, Param_Cell);
	g_fwdOnClientLoaded = CreateGlobalForward("Kraken_OnClientLoaded", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);
	g_fwdOnClientUnload = CreateGlobalForward("Kraken_OnClientUnload", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_String);
	g_fwdOnDatabaseDisconnect = CreateGlobalForward("Kraken_OnDatabaseDisconnect", ET_Ignore);
	g_fwdOnVersionChecked = CreateGlobalForward("Kraken_OnVersionChecked", ET_Ignore, Param_String, Param_String, Param_String);
	
	/* Natives: Database */
	
	CreateNative("Kraken_IsDatabaseLoaded", Native_IsDatabaseLoaded);
	CreateNative("Kraken_GetDatabase", Native_GetDatabase);
	CreateNative("Kraken_PingDatabase", Native_PingDatabase);
	CreateNative("Kraken_PushQuery", Native_PushQuery);
	
	/* Natives: Server Info */
	
	CreateNative("Kraken_GetServerID", Native_GetServerID);
	CreateNative("Kraken_GetServerGroupID", Native_GetServerGroupID);
	CreateNative("Kraken_GetServerSlots", Native_GetServerSlots);
	CreateNative("Kraken_GetServerName", Native_GetServerName);
	CreateNative("Kraken_GetServerIP", Native_GetServerIP);
	CreateNative("Kraken_GetServerPort", Native_GetServerPort);
	
	/* Natives:  Settings */
	
	CreateNative("Kraken_RegisterSetting", Native_RegisterSetting);
	CreateNative("Kraken_GetSettingInt", Native_GetSettingInt);
	CreateNative("Kraken_GetSettingFloat", Native_GetSettingFloat);
	CreateNative("Kraken_GetSettingString", Native_GetSettingString);
	CreateNative("Kraken_SetSettingInt", Native_SetSettingInt);
	CreateNative("Kraken_SetSettingFloat", Native_SetSettingFloat);
	CreateNative("Kraken_SetSettingString", Native_SetSettingString);
	
	/* Natives: Server Settings */
	
	CreateNative("Kraken_RegisterServerSetting", Native_RegisterServerSetting);
	CreateNative("Kraken_GetServerSettingInt", Native_GetServerSettingInt);
	CreateNative("Kraken_GetServerSettingFloat", Native_GetServerSettingFloat);
	CreateNative("Kraken_GetServerSettingString", Native_GetServerSettingString);
	CreateNative("Kraken_SetServerSettingInt", Native_SetServerSettingInt);
	CreateNative("Kraken_SetServerSettingFloat", Native_SetServerSettingFloat);
	CreateNative("Kraken_SetServerSettingString", Native_SetServerSettingString);
	
	/* Natives: Map */
	
	CreateNative("Kraken_GetMapID", Native_GetMapID);
	
	/* Natives: Player */
	
	CreateNative("Kraken_GetUserID", Native_GetUserID);
	CreateNative("Kraken_GetUserSessionID", Native_GetUserSessionID);
	
	/* Admin Commands */
	
	RegAdminCmd("kraken_setting_set", AdminCmd_SettingSet, ADMFLAG_ROOT);
	RegAdminCmd("kraken_setting_list", AdminCmd_SettingList, ADMFLAG_ROOT);
	
	RegAdminCmd("kraken_server_setting_set", AdminCmd_ServerSettingSet, ADMFLAG_ROOT);
	RegAdminCmd("kraken_server_setting_list", AdminCmd_ServerSettingList, ADMFLAG_ROOT);
	
	LoadTranslations("common.phrases");
	
	Kraken_MarkNativesAsOptional();

	return APLRes_Success;
}