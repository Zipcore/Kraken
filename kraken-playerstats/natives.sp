public int Native_GetStatsCount(Handle hPlugin, int iParams) 
{
	return STATS_COUNT;
}

public int Native_GetStatsID(Handle hPlugin, int iParams) 
{
	char sName[10];
	GetNativeString(1, sName, sizeof(sName));
	return GetStatsID(sName);
}

public int Native_GetStatsName(Handle hPlugin, int iParams) 
{
	int iSID = GetNativeCell(1);
	int iLength = GetNativeCell(3);
	
	if (iLength < 1)
		return false;
	
	if(iSID < 0 || iSID >= STATS_COUNT)
		return false;
	
	if (SetNativeString(4, g_sStats[iSID], iLength, true) == SP_ERROR_NONE)
		return true;
	
	return false;
}

public int Native_StatsLoaded(Handle hPlugin, int iParams) 
{
	return g_bClientLoaded[GetNativeCell(1)];
}

public int Native_GetStats(Handle hPlugin, int iParams) 
{
	return g_iStats[GetNativeCell(1)][GetNativeCell(2)];
}