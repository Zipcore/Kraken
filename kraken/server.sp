void InitServer()
{
	char sBuffer[2048];
	
	FormatEx(sBuffer, sizeof(sBuffer), "CREATE TABLE IF NOT EXISTS `servers` (sid INT NOT NULL AUTO_INCREMENT, gid INT DEFAULT -1, name VARCHAR(64) NOT NULL, ip VARCHAR(15) NOT NULL, port INT(5) NOT NULL, slots INT(4) DEFAULT -1, PRIMARY KEY (`sid`, `ip`, `port`), INDEX gid_index USING BTREE (gid), INDEX ip_index USING BTREE (ip), INDEX port_index USING BTREE (port));");
	g_dMain.Query(Callback_CreateTable, sBuffer, _, DBPrio_High);
	
	FormatEx(sBuffer, sizeof(sBuffer), "CREATE TABLE IF NOT EXISTS `server_groups` (gid INT NOT NULL AUTO_INCREMENT, name VARCHAR(16) NOT NULL, PRIMARY KEY (`gid`, `name`), INDEX gid_index USING BTREE (gid));");
	g_dMain.Query(Callback_CreateTable, sBuffer, _, DBPrio_High);
}

void UpdateHostInfo()
{
	// Hostname
	FindConVar("hostname").GetString(g_sHostName, sizeof(g_sHostName));
	
	// Hostport
	g_iHostPort = FindConVar("hostport").IntValue;
	
	// IP Method #1
	if(FindConVar("net_public_adr") != null)
		GetConVarString(FindConVar("net_public_adr"), g_sHostIP, sizeof(g_sHostIP));
	
	// IP Method #2
	if(strlen(g_sHostIP) == 0 && FindConVar("ip") != null)
		GetConVarString(FindConVar("ip"), g_sHostIP, sizeof(g_sHostIP));
	
	// IP Method #3
	if((strlen(g_sHostIP) == 0 || StrEqual(g_sHostIP, "0.0.0.0")) && FindConVar("hostip") != null)
	{
		int ip = GetConVarInt(FindConVar("hostip"));
		FormatEx(g_sHostIP, sizeof(g_sHostIP), "%d.%d.%d.%d", (ip >> 24) & 0x000000FF, (ip >> 16) & 0x000000FF, (ip >> 8) & 0x000000FF, ip & 0x000000FF);
	}
}

public void LoadServerInfo() 
{
	if(g_bServerLoaded)
	{
		Call_StartForward(g_fwdOnDatabaseDisconnect);
		Call_Finish();
		
		g_bServerLoaded = false;
	}
	
	char sBuffer[512];
	
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT sid, gid, slots, name FROM `servers` WHERE ip = '%s' AND port = '%i';", g_sHostIP, g_iHostPort);
	g_dMain.Query(Callback_LoadServerInfo, sBuffer, _, DBPrio_High);
}

public void Callback_LoadServerInfo(Database db, DBResultSet results, const char[] error, any data)
{
	if (db == null) 
	{
		LogError("(LoadServerInfo) - %s", error);
		return;
	}
	
	if(results.FetchRow())
	{
		g_iServerID = results.FetchInt(0);
		
		g_iServerGroupID = results.FetchInt(1);
		
		g_iServerSlots = results.FetchInt(2);
		
		if(g_iServerSlots == -1)
			g_iServerSlots = 64;
		
		results.FetchString(3, g_sHostName, sizeof(g_sHostName));
		
		OnServerInfoLoaded();
		
		return;
	}
	
	g_iServerID = -1;
	g_iServerGroupID = -1;
	
	InsertServer();
	LoadServerInfo();
}

void InsertServer()
{
	char sBuffer[512];
	
	FormatEx(sBuffer, sizeof(sBuffer), "INSERT INTO `servers` (ip, port, name)  VALUES ('%s', '%i', '%s');", g_sHostIP, g_iHostPort, g_sHostName);
	g_dMain.Query(Callback_InsertServer, sBuffer, _, DBPrio_High);
}

public void Callback_InsertServer(Database db, DBResultSet results, const char[] error, any data)
{
	if (db == null) 
	{
		LogError("(InsertServer) - %s", error);
		return;
	}
}

/* Natives */

public int Native_GetServerID(Handle hPlugin, int iParams) 
{
	return g_iServerID;
}

public int Native_GetServerGroupID(Handle hPlugin, int iParams) 
{
	return g_iServerGroupID;
}

public int Native_GetServerSlots(Handle hPlugin, int iParams) 
{
	return g_iServerSlots;
}

public int Native_GetServerName(Handle hPlugin, int iParams) 
{
	SetNativeString(1, g_sHostName, GetNativeCell(2));
}

public int Native_GetServerIP(Handle hPlugin, int iParams) 
{
	SetNativeString(1, g_sHostIP, GetNativeCell(2));
}

public int Native_GetServerPort(Handle hPlugin, int iParams) 
{
	return g_iHostPort;
}