/* OnPluginStart */

public void OnPluginStart() 
{
	CreateConvars();
	ConnectDB();
	
	HookEvent("player_disconnect", Event_OnDisconnect);
}

/* OnMapStart */

public void OnMapStart()
{
	MapStart();
}

/* OnMapEnd */

public void OnMapEnd()
{
	ResetAuthtimer();
}

/* OnDatabaseLoaded */

void OnDatabaseLoaded()
{
	LogMessage("Booting Kraken - Step 1 / 5 - Database Connected.");
	
	InitServer();
	InitServerSettings();
	InitMap();
	InitUser();

	LoadServerInfo();
}

/* OnServerInfoLoaded */

void OnServerInfoLoaded()
{
	LogMessage("Booting Kraken - Step 2 / 5 - Server info loaded (ID: %i).", g_iServerID);
	
	g_bServerLoaded = true;
	
	LoadSettings();
}

/* OnSettingsLoaded */

void OnSettingsLoaded()
{
	LogMessage("Booting Kraken - Step 3 / 5 - Kraken settings loaded.");
	
	CheckPluginVersion();
}

/* OnPluginVersionChecked */

void OnPluginVersionChecked(char[] sPluginVersion, char[] sFileVersion, char[] sDBVersion)
{
	LogMessage("Booting Kraken - Step 4 / 5 - Version checked (Plugin: %s | File: %s | DB: %s) (4 / 5)", sPluginVersion, sFileVersion, sDBVersion);
	
	LoadMapID();
	LoadServerSettings();
}

/* OnSettingsLoaded */

void OnServerSettingsLoaded() 
{
  	LogMessage("Booting Kraken - Step 5 / 5 - Server settings loaded.");
	
	/* Late load players */
	for (int iClient = 1; iClient <= MaxClients; iClient++)
		OnClientPostAdminCheck(iClient);
	
	/* Announce that database is ready */
	Call_StartForward(g_fwdOnDatabaseConnected);
	Call_PushCell(g_dMain);
	Call_PushCell(g_iServerID);
	Call_PushCell(g_iServerGroupID);
	Call_PushString(g_sHostName);
	Call_Finish();
}

/* OnClientPostAdminCheck */

public void OnClientPostAdminCheck(int iClient) 
{
	if(Kraken_IsValidClient(iClient))
		LoadUserID(iClient, true);
}

/* Kraken_OnClientUnload */

public void Kraken_OnClientUnload(int iClient, int iUserID, int iSessionID, char[] sDcReason)
{
	
}