public void LoadUserID(int iClient, bool delay) 
{
	if(delay || !g_bServerLoaded || !GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
	{
		g_hAuthTimer[iClient] = CreateTimer(1.5+0.025*float(iClient), Timer_PostAdminCheck, iClient, TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT * FROM `players` WHERE steamid = '%s';", g_sAuth[iClient]);
	g_dMain.Query(Callback_LoadUserID, sBuffer, GetClientUserId(iClient), DBPrio_High);
}

public Action Timer_PostAdminCheck(Handle hTimer, int iClient) 
{
	if(g_hAuthTimer[iClient] != hTimer)
	{
		LogMessage("There is another auth timer already running.");
		g_hAuthTimer[iClient] = null;
		return Plugin_Handled;
	}
	
	g_hAuthTimer[iClient] = null;
	
	if(g_bUserLoaded[iClient])
		return Plugin_Handled;

	if (Kraken_IsValidClient(iClient))
	{
		LoadUserID(iClient, !g_bServerLoaded);
		return Plugin_Handled;
	}

	return Plugin_Handled;
}

public void Callback_LoadUserID(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(LoadUserID) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if (!Kraken_IsValidClient(iClient)) 
		return;
		
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
	{
		g_hAuthTimer[iClient] = CreateTimer(1.0, Timer_PostAdminCheck, GetClientUserId(iClient), TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	if (results.FetchRow()) 
	{
		g_iUserID[iClient] = results.FetchInt(0);
		
		if(!GetClientIP(iClient, g_sIP[iClient], sizeof(g_sIP[])))
		{
			g_sIP[iClient] = "Unknown";
			g_sCCode[iClient] = "???";
		}
		else if(!GeoipCode3(g_sIP[iClient], g_sCCode[iClient]))
			g_sCCode[iClient] = "???";
		
		StartSession(iClient);
		UpdatePlayerName(iClient);
		
		return;
	}
	
	InsertUser(iClient);
}

void InsertUser(int iClient)
{
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
		return;
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "INSERT INTO `players` (steamid, firstjoin) VALUES ('%s', CURRENT_TIMESTAMP);", g_sAuth[iClient]);
	g_dMain.Query(Callback_InsertUser, sBuffer, GetClientUserId(iClient), DBPrio_High);
}

public void Callback_InsertUser(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(InsertUser) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if (!Kraken_IsValidClient(iClient)) 
		return;
	
	LoadUserID(iClient, false);
}

void ResetAuthtimer()
{
	for (int iClient = 1; iClient <= MaxClients; iClient++)
		g_hAuthTimer[iClient] = null;
}