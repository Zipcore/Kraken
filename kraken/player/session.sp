void StartSession(int iClient)
{
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "INSERT INTO `player_session` (uid, sid, ip, country, dcreason) VALUES ('%i','%i','%s','%s','%s');", g_iUserID[iClient], g_iServerID, g_sIP[iClient], g_sCCode[iClient], DCREASON_CONNECTED);
	g_dMain.Query(Callback_StartSession, sQuery, GetClientUserId(iClient), DBPrio_High);
}

public void Callback_StartSession(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(StartSession) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if (!Kraken_IsValidClient(iClient)) 
		return;
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "SELECT MAX(id) FROM `player_session` WHERE uid = '%i';", g_iUserID[iClient]);
	g_dMain.Query(Callback_LoadSessionID, sQuery, GetClientUserId(iClient), DBPrio_High);
}

public void Callback_LoadSessionID(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(LoadSessionID) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if (!Kraken_IsValidClient(iClient)) 
		return;
		
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
	{
		g_hAuthTimer[iClient] = CreateTimer(1.0, Timer_PostAdminCheck, GetClientUserId(iClient), TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	if (results.FetchRow()) 
		g_iUserSessionID[iClient] = results.FetchInt(0);
	
	g_bUserLoaded[iClient] = true;
		
	Call_StartForward(g_fwdOnClientLoaded);
	Call_PushCell(iClient);
	Call_PushCell(g_iUserID[iClient]);
	Call_PushCell(g_iUserSessionID[iClient]);
	Call_Finish();
}