stock bool String_EndsWith(const char[] str, const char[] subString)
{
	int n_str = strlen(str) - 1;
	int n_subString = strlen(subString) - 1;

	if(n_str < n_subString) {
		return false;
	}

	while (n_str != 0 && n_subString != 0) {

		if (str[n_str--] != subString[n_subString--]) {
			return false;
		}
	}

	return true;
}

stock void FormatBanTime(int iTime, char[] sOutput, int iLength)
{
	if(iTime < 0)
		Format(sOutput, iLength, "Session");
	else if(iTime == 0)
		Format(sOutput, iLength, "Permanent");
	else if (iTime >= 525600)
	{
		int years = RoundToFloor(iTime / 525600.0);
		Format(sOutput, iLength, "%d mins (%d year%s)", iTime, years, years == 1 ? "" : "s");
    }
	else if (iTime >= 10080)
	{
		int weeks = RoundToFloor(iTime / 10080.0);
		Format(sOutput, iLength, "%d mins (%d week%s)", iTime, weeks, weeks == 1 ? "" : "s");
    }
	else if (iTime >= 1440)
	{
		int days = RoundToFloor(iTime / 1440.0);
		Format(sOutput, iLength, "%d mins (%d day%s)", iTime, days, days == 1 ? "" : "s");
    }
	else if (iTime >= 60)
	{
		int hours = RoundToFloor(iTime / 60.0);
		Format(sOutput, iLength, "%d mins (%d hour%s)", iTime, hours, hours == 1 ? "" : "s");
    }
	else if (iTime > 0) Format(sOutput, iLength, "%d min%s", iTime, iTime == 1 ? "" : "s");
}