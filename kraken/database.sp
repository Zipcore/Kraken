public void ConnectDB() 
{
	UpdateHostInfo();
	
	if(!SQL_CheckConfig("kraken"))
		SetFailState("Invalid or couldn't find database configuration for \"kraken\" inside \"addons/sourcemod/configs/databases.cfg\"");
	
	Database.Connect(Callback_ConnectDB, "kraken");
}

public void Callback_ConnectDB(Database db, const char[] error, int data)
{
	if (db == null) 
	{
		SetFailState("(ConnectDB-Kraken) - %s", error);
		return;
	}
	
	g_dMain = db;
	g_dMain.SetCharset("utf8mb4");
	CreateKrakenTable();
	CreateServerSettingsTable();
}

public void Callback_CreateTable(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(CreateTable) - %s", error);
		return;
	}
}

void CreateKrakenTable()
{
	char sQuery[2048];
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `kraken` (feature VARCHAR(64) NOT NULL, setting VARCHAR(512) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`feature`));");
	g_dMain.Query(Callback_CreateVersionTable, sQuery, _, DBPrio_High);
}

public void Callback_CreateVersionTable(Database db, DBResultSet results, const char[] error, any data)
{
	if (db == null) 
	{
		LogError("(Callback_CreateVersionTable) - %s", error);
		return;
	}
	
	OnDatabaseLoaded();
}

/* Natives */

public int Native_PushQuery(Handle hPlugin, int iParams)
{
	int iLength = -1;
	if(GetNativeStringLength(1, iLength) != SP_ERROR_NONE || iLength < 1)
		return false;
	
	char[] sBuffer = new char[iLength];
	if(GetNativeString(1, sBuffer, iLength+1) != SP_ERROR_NONE)
		return false;
	
	g_dMain.Query(Callback_PushQuery, sBuffer, _, DBPrio_Low);
		
	return true;
}

public void Callback_PushQuery(Database db, DBResultSet results, const char[] error, any data) 
{
	if (db == null) 
	{
		LogError("(PushQuery) - %s", error);
		return;
	}
}

/* Natives */

public int Native_IsDatabaseLoaded(Handle hPlugin, int iParams) 
{
	return g_bServerLoaded;
}

public int Native_GetDatabase(Handle hPlugin, int iParams) 
{
	return view_as<any>(g_dMain);
}

public int Native_PingDatabase(Handle hPlugin, int iParams) 
{
	Call_StartForward(g_fwdOnDatabasePong);
	Call_PushCell(g_dMain);
	Call_PushCell(g_iServerID);
	Call_PushCell(g_iServerGroupID);
	Call_PushString(g_sHostName);
	Call_Finish();
}
