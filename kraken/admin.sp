public Action AdminCmd_SettingSet(int iClient, int iArgs)
{
	if(iArgs != 2)
	{
		CReplyToCommand(iClient, "%s Usage: sm_kraken_setting_set <feature> <setting>", g_sChatPrefixAdmin);
		return Plugin_Handled;
	}
	
	char sFeature[128], sSetting[128];
	GetCmdArg(1, sFeature, sizeof(sFeature));
	GetCmdArg(2, sSetting, sizeof(sSetting));
	
	if(strlen(sFeature) > 2 && strlen(sSetting) > 2)
	{
		SetSetting(sFeature, sSetting);
		CReplyToCommand(iClient, "%s Set \"%s\" to \"%s\"", g_sChatPrefixAdmin, sFeature, sSetting);
	}
	
	return Plugin_Handled;
}

public Action AdminCmd_SettingList(int iClient, int iArgs)
{
	CReplyToCommand(iClient, "%s Features cached: %i", g_sChatPrefixAdmin, g_aFeatures.Length);
	
	char sFeature[512], sSettings[64];
	
	for (int iIndex = 0; iIndex < g_aFeatures.Length; iIndex++)
	{
		g_aFeatures.GetString(iIndex, sFeature, sizeof(sFeature));
		g_aSettings.GetString(iIndex, sSettings, sizeof(sSettings));
		CReplyToCommand(iClient, "%s #%i> %s: %s", g_sChatPrefixAdmin, iIndex, sFeature, sSettings);
	}
	
	return Plugin_Handled;
}



public Action AdminCmd_ServerSettingSet(int iClient, int iArgs)
{
	if(iArgs != 2)
	{
		CReplyToCommand(iClient, "%s Usage: sm_kraken_server_setting_set <feature> <setting>", g_sChatPrefixAdmin);
		return Plugin_Handled;
	}
	
	char sFeature[128], sSetting[128];
	GetCmdArg(1, sFeature, sizeof(sFeature));
	GetCmdArg(2, sSetting, sizeof(sSetting));
	
	if(strlen(sFeature) > 2 && strlen(sSetting) > 2)
	{
		SetServerSetting(sFeature, sSetting);
		CReplyToCommand(iClient, "%s Set \"%s\" to \"%s\"", g_sChatPrefixAdmin, sFeature, sSetting);
	}
	
	return Plugin_Handled;
}

public Action AdminCmd_ServerSettingList(int iClient, int iArgs)
{
	CReplyToCommand(iClient, "%s Features cached: %i", g_sChatPrefixAdmin, g_aFeatures.Length);
	
	char sFeature[512], sSettings[64];
	
	for (int iIndex = 0; iIndex < g_aFeatures.Length; iIndex++)
	{
		g_aServerFeatures.GetString(iIndex, sFeature, sizeof(sFeature));
		g_aServerSettings.GetString(iIndex, sSettings, sizeof(sSettings));
		CReplyToCommand(iClient, "%s #%i> %s: %s", g_sChatPrefixAdmin, iIndex, sFeature, sSettings);
	}
	
	return Plugin_Handled;
}